package com.example.app3.data

data class Oil(val name: String, val price: Double)
